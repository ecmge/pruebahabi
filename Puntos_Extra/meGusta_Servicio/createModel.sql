CREATE TABLE users (
    user_id INT PRIMARY KEY,
    name VARCHAR(255),
    age INT,
    username VARCHAR(255)
);

CREATE TABLE properties (
    property_id INT PRIMARY KEY,
    address VARCHAR(255),
    city VARCHAR(100),
    price INT,
    description VARCHAR(255),
    year INT
);

CREATE TABLE likes (
    like_id INT PRIMARY KEY,
    user_id INT,
    property_id INT,
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (property_id) REFERENCES properties(property_id)
);