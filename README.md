# Prueba Habi

## 1. Tecnologías a usar y solución:

Para la solución de la presente prueba lo planteare de este modo, primero debo leer la peticion en formato JSON para saber los filtros que mi usuario quiere para ver las propiedades,
posterior a eso debo realizar una conexion a la BBDD y ejecutar una query donde agregue los respectivos filtros que el usuario me indico, luego deberia tomar dicho resultado 
serializarlo (convertirlo en un JSON) y finalmente enviarlo mediante un canal http.

### Que voy a usar?

- **Python 3.10:** Como versión de código.
- **Libreria MYSQL:** Para conectarme a la db y ejecutar las querys alli.
- **Libreria Flake8:** Para el estándar de las normas PEP8.
- **Librería JSON:** Para leer la peticion JSON y asi mismo crearla.
- **Librería HTTP:** Para crear el canal de comunicación y hacer el envio de la petición.
- **Libreria Pytest:** Para los test unitarios de la app.
- **Libreria Variables de entorno:** Libreria decouple para lectura de variables de entorno.

## 2. Cómo funciona:

- Clonar el repo.
- Por cuestiones teoricas, el archivo .env para usar variables de entorno estara en el repo, pero por buena practica esto 
normalmente no es asi.
- Crear un entorno virtual, activarlo y ejecutar "pip install -r requirements.txt".
- Verificar no tener en uso el puerto 8000.
- Ejecutar el main.py, el cual enviará un mensaje de ejecución.
- Ir al archivo data.json y modificar los filtros según lo que se quiera ver, ciudad, año y estado.
- Ir a Postman, el mismo navegador u otro servicio de peticiones y hacer un GET a la URL localhost:8000, si
se usa el mismo navegador solo ir a la URL localhost:8000.
- Verificar la información obtenida, que se entrega en un archivo JSON.

## Testing

- Se utilizó la librería pytest por ende para ejecutar las pruebas unitarias realizadas basta con
ejecutar el comando "pytest" en la consola para verificar las pruebas.


## Explicación de Servicio de Me Gusta:

Para este punto de la prueba lo que pense para ejecutar dicho modelo
fue básicamente crear una tabla intermedia entre user y property llamada likes, la cual tendrá inicialmente
dos llaves primarias (PK), y una variable status. Las llaves para conectar al usuario con la propiedad
y así saber qué usuario le dio "like" a la propiedad y así mismo el status para saber si el like del usuario
está activo (1) o decidió retirarlo por algún motivo (0). Con esto sí queremos totalizar los "likes" de cada propiedad
con una consulta podemos obtener los likes activos de cada propiedad y posteriormente mostrarlos en nuestro front.