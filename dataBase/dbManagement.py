import mysql.connector
from decouple import config

# Funcion para conectarse a la DB MySQL utilizando variables de entorno.


def connect_database():
    try:
        connector = mysql.connector.connect(
            host=config('MYSQL_HOST'),
            port=config('MYSQL_PORT'),
            user=config('MYSQL_USER'),
            password=config('MYSQL_PASSWORD'),
            database=config('MYSQL_DATABASE'),
            autocommit=True
        )
        print("Conexión exitosa a la Base de Datos")
        return connector

    except mysql.connector.Error as err:
        print("Error en la conexión a la Base de Datos:", err)
        return None


"""
Funcion donde ejecutamos una query de la base de datos para obtener la
respectiva informacion, agregando los filtros que llegaron del archivo
JSON, posterior ejecutamos y retorna los datos que se encontraron en la DB.
"""


def properties_info(filters):
    query = ("SELECT "
             "p.address, "
             "p.city, "
             "sh.name, "
             "p.price, "
             "p.description, "
             "sh.status "
             "FROM "
             "property p "
             "LEFT JOIN ( "
             "SELECT "
             "property_id, "
             "MAX(update_date) AS last_update_date, "
             "s.name, "
             "sh.status_id status "
             "FROM "
             "status_history sh "
             "LEFT JOIN "
             "status s ON sh.status_id = s.id "
             "WHERE "
             "status_id IN (3, 4, 5) "
             "GROUP BY "
             "property_id "
             ") sh ON p.id = sh.property_id ")
    query_filters = []
    for key, value in filters.items():
        if value != "":
            query_filters.append("{} = {}".format(
                key, value if value.isnumeric() else f"'{value}'"))
    if len(query_filters):
        query += "WHERE " + " AND ".join(query_filters)
    conector = connect_database()
    cursor = conector.cursor()
    cursor.execute(query)
    results = cursor.fetchall()
    return results
