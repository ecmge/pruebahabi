from http.server import BaseHTTPRequestHandler, HTTPServer
from request.userRequest import user_request

# Clase con la que manejaremos las solicitues HTTP.


class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        results = user_request('../Habi_Test_ECM/data.json')
        self.wfile.write(results.encode())

# Creacion del servidor para recibir las peticiones.


def main():
    server_address = ('', 8000)
    httpd = HTTPServer(server_address, RequestHandler)
    print('....Servidor HTTP iniciado...')
    httpd.serve_forever()


if __name__ == '__main__':
    main()
