import json
from dataBase.dbManagement import properties_info

""""
Funcion que se encarga de procesar los datos provenientes de un archivo JSON
recibe el path donde se encuentra el archivo JSON con los filtros a aplicar
y regresa un resultado de la consulta con dichos filtros.
"""


def user_request(file_path):
    try:
        with open(file_path, 'r') as file:
            data = file.read()
            filters = json.loads(data)
            results = properties_info(filters)
            info = user_serializer(results)
    except FileNotFoundError:
        print('El archivo no se encontró')
    except Exception as e:
        print('Ocurrió un error:', e)
    return info

# Funcion para serializar los datos y reenviarlos en un formato JSON al usuario


def user_serializer(results):
    serializer_results = []
    for row in results:
        serializer_results.append({
            "address": row[0],
            "city": row[1],
            "status": row[2],
            "price": row[3],
            "description": row[4]
        })

    return json.dumps(serializer_results)
