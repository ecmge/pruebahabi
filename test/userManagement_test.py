import pytest
from dataBase.dbManagement import properties_info
from request.userRequest import user_serializer


@pytest.fixture
def dummy_filters():
    return {
        "city": "bogota",
        "year": 2000,
        "status": "3"
    }


@pytest.fixture
def dummy_results():
    return [
        ("Call 100 con 9na", "bogota", "pre_venta", 20000000,
         "Hermoso apto en edidico al norte", 3),
        ("6D con 23", "barranquilla", "en_venta", 20010000,
         "Casa cerca a la playa", 4)
    ]

# Verificamos que la funcion este retornando una lista


def test_properties_info_return(dummy_filters):
    results = properties_info(dummy_filters)
    assert isinstance(results, list)


"""
Verificamos que la estructura del retorno sean los 6 campos esperados en la
consulta SQL (Dirección, Ciudad, Estado, Precio de venta,
Descripción y ID de Estado.)
"""


def test_properties_info_sql_structure(dummy_filters):
    results = properties_info(dummy_filters)
    for row in results:
        assert isinstance(row, tuple)
        assert len(row) == 6

# Esta prueba verifica si el contenido del JSON devuelto es correcto


def test_user_serializer_correct_content(dummy_results):
    expected_json = ('[{"address": "Call 100 con 9na", "city": "bogota",'
                     ' "status": "pre_venta",' ' "price": 20000000, '
                     '"description": "Hermoso apto en edidico al norte"},'
                     ' {"address": "6D con 23", "city": "barranquilla",'
                     ' "status": "en_venta",' ' "price": 20010000,'
                     ' "description": "Casa cerca a la playa"}]')
    serialized_data = user_serializer(dummy_results)
    assert serialized_data == expected_json
